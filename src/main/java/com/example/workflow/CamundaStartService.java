package com.example.workflow;

import com.example.workflow.dto.BookingTransaction;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.variable.Variables;
import org.camunda.bpm.engine.variable.value.ObjectValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CamundaStartService {
    @Autowired
    RuntimeService runtimeService;

    public void startProcessByMessage(BookingTransaction bt) throws Exception {
        ObjectMapper obj = new ObjectMapper();
        String json = obj.writeValueAsString(bt);
        runtimeService.createMessageCorrelation("msg_start")
                .setVariable("bookingJson", json)
                .correlate();
    }
}
