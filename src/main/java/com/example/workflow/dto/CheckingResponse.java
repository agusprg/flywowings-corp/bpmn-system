package com.example.workflow.dto;

import lombok.Data;

@Data
public class CheckingResponse {
    Integer status;
    String message;
}
