package com.example.workflow.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BookingTransaction implements Serializable {

    private String transactionId;
    private String bookingCode;
    private String channel;
    private String email;
    private String name;
    private String contact;
    private String flightCode;
    private Date flightDate;
    private Time departure;
    private Time arrival;
    private Double price;
    private Integer totalPassanger;
    private Double totalPrice;
    private Date transactionDate;
    private String transactionStatus;
    List<BookingPassanger> passanger;
    String callback;
    String channelCallback;

}