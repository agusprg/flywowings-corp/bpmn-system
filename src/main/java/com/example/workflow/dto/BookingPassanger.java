package com.example.workflow.dto;

import lombok.Data;

import java.util.Date;

@Data
public class BookingPassanger {
    private String firstName;
    private String lastName;
    private String identityNumber;
    private Date DOB;
}
