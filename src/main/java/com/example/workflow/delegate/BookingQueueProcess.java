package com.example.workflow.delegate;

import com.example.workflow.dto.BookingTransaction;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;
@Component
@Slf4j
public class BookingQueueProcess implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) throws JsonProcessingException {
        log.info("[CAMUNDA] start queueing booking");
        ObjectMapper mapper = new ObjectMapper();
        String json = (String) execution.getVariable("bookingJson");
        BookingTransaction map = mapper.readValue(json, BookingTransaction.class);
        log.info("[CAMUNDA] end queueing booking {}",map);
    }
}