package com.example.workflow.delegate;

import com.example.workflow.dto.BookingPassanger;
import com.example.workflow.dto.BookingTransaction;
import com.example.workflow.dto.CheckingResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CheckingDukcapil implements JavaDelegate {

    private final RestTemplate restTemplate;

    @Value("${endpoint.dukcapil}")
    private String endpoint;

    @Override
    public void execute(DelegateExecution execution) throws JsonProcessingException {
        log.info("[CAMUNDA] start checking dukcapil");
        ObjectMapper mapper = new ObjectMapper();
        String json = (String) execution.getVariable("bookingJson");
        BookingTransaction map = mapper.readValue(json, BookingTransaction.class);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(List.of(MediaType.APPLICATION_JSON));

        String jsonPSG = mapper.writeValueAsString(map.getPassanger());

        HttpEntity<String> entity = new HttpEntity<>(jsonPSG, headers);
        CheckingResponse obj = restTemplate.exchange(endpoint, HttpMethod.POST, entity, CheckingResponse.class).getBody();

        assert obj != null;
        if(0 == obj.getStatus())
            execution.setVariable("isGreen",true);
        else execution.setVariable("isGreen",false);

        execution.setVariable("state","DUKCAPIL");
        log.info("[CAMUNDA] end checking dukcapil");
    }
}
