package com.example.workflow.delegate;

import com.example.workflow.dto.BookingTransaction;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ExpiryCallback implements JavaDelegate {

    private final RestTemplate restTemplate;

    @Override
    public void execute(DelegateExecution execution) throws JsonProcessingException {
        log.info("[CAMUNDA] start notify expired callback");
        ObjectMapper mapper = new ObjectMapper();
        String json = (String) execution.getVariable("bookingJson");
        BookingTransaction bt = mapper.readValue(json, BookingTransaction.class);

        Map<String,Object> body = new HashMap<>();
        body.put("transaction_id", bt.getTransactionId());
        body.put("transaction_status", "EXPIRED");
        body.put("channel_callback", bt.getChannelCallback());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(List.of(MediaType.APPLICATION_JSON));

        HttpEntity<Map<String,Object>> entity = new HttpEntity<>(body, headers);
        restTemplate.exchange(bt.getCallback(), HttpMethod.POST, entity, Object.class).getBody();

        log.info("[CAMUNDA] end notify expired callback");
    }
}

