package com.example.workflow;

import com.example.workflow.dto.BookingTransaction;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@RestController
@Slf4j
public class CamundaController {

    @Autowired
    CamundaStartService camundaStartService;

    private static ExecutorService executorService = Executors.newCachedThreadPool();


    @PostMapping("/process")
    public void persistPerson(@RequestBody BookingTransaction bt) throws Exception {
        executorService.submit(()-> {
            try{
                camundaStartService.startProcessByMessage(bt);
            }catch (Exception e){
                log.error("error send to camunda");
            }
        });

    }
}
